#!/usr/bin/env bash
echo "Installing Wireguard"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install raspberrypi-kernel-headers
echo "deb http://deb.debian.org/debian/ unstable main" | sudo tee --append /etc/apt/sources.list.d/unstable.list
sudo apt-get install dirmngr
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 648ACFD622F3D138
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 150\n' | sudo tee --append /etc/apt/preferences.d/limit-unstable
sudo apt-get update
sudo apt-get install wireguard -y
echo "Configuring Wireguard"
sudo perl -pi -e 's/#{1,}?net.ipv4.ip_forward ?= ?(0|1)/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
sudo sysctl -p
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
umask 077
echo Generating Keys
wg genkey | tee server_private_key | wg pubkey > server_public_key
server_private_key=$(cat server_private_key)
server_public_key=$(cat server_public_key)
if_wan=$(route | grep '^default' | grep -o '[^ ]*$')
## Create if it doesn-t exist the diurectory
[ -d /etc/wireguard ] || sudo mkdir /etc/wireguard
sudo sh -c 'cat  > /etc/wireguard/wg0.conf << EOF
[Interface]
Address = 10.0.0.1/24
# SaveConfig to false to keep comments
SaveConfig = false
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o if_wan -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o if_wan -j MASQUERADE
ListenPort = 51820
PrivateKey = <server_private_key>

[Peer]
PublicKey = <client_public_key>
AllowedIPs = 10.0.0.2/32
EOF'
sudo sed -i.bak "s#<server_private_key>#$server_private_key#g" /etc/wireguard/wg0.conf
sudo sed -i.bak "s/if_wan/$if_wan/g" /etc/wireguard/wg0.conf



echo "Client configuration"
cat > wg0-client.conf << EOF
[Interface]
Address = 10.0.0.2/32
# SaveConfig to false to keep comments
SaveConfig = false
DNS = 8.8.8.8
PrivateKey = <client_private_key>

[Peer]
# Examples: Host: 192.168.1.101, Net: 192.168.1.0/24, All: 0.0.0.0/0, ::/0
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = <SERVER_PUBLIC_IP>:51820
PublicKey = <SERVER_PUBLIC_KEY>
EOF


sed -i.bak "s#<SERVER_PUBLIC_KEY>#$server_public_key#g" wg0-client.conf
cat wg0-client.conf
echo "Enter here the Client public key"
read ClientPublicKey
sudo sed -i.bak "s#<client_public_key>#$ClientPublicKey#g" /etc/wireguard/wg0.conf
echo "Enabling service"
sudo systemctl enable wg-quick@wg0.service
echo "Starting service" 
sudo systemctl start wg-quick@wg0.service 
